import oss2
import json

class AlibabaAgent:
    def __init__(self):
        with open("config/alibaba_config.json") as json_data_file:
            self.alibaba_data = json.load(json_data_file)
        print("Authenticating")
        self.auth = oss2.Auth(self.alibaba_data["access_key"], self.alibaba_data["secret_key"])
        print("Connecting to bucket")

    def percentage(self, consumed_bytes, total_bytes):
        if total_bytes:
            rate = int(100 * (float(consumed_bytes) / float(total_bytes)))
            print('\rDownloading Model {0}% '.format(rate), end='')

    def download(self, bucket_name, file, save_file_path):
        bucket = oss2.Bucket(self.auth, "http://oss-"+ self.alibaba_data["region"]+".aliyuncs.com", bucket_name)
        print("Authorized OSS Bucket ", bucket_name)
        print("Downloading the fused onnx model...")
        bucket.get_object_to_file(file, save_file_path, progress_callback=self.percentage)


    def upload(self, bucket_name, compiled_file_path, save_file_path_to_oss):
        bucket = oss2.Bucket(self.auth, "http://oss-"+ self.alibaba_data["region"]+".aliyuncs.com", bucket_name)
        bucket.put_object(save_file_path_to_oss, open(compiled_file_path, "rb"))
        print("TVM converted Model Uploded to OSS bucket")

        