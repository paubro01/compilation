import sys
import os
import subprocess
import json
import argparse
from flask import Flask
from flask import send_file
from src.argconfig import parse_args
from src.alibaba_agent import AlibabaAgent
from src.aws_agent import AwsAgent
from src.onnx_to_tvm import onnx_to_tvm

app = Flask(__name__)
ap = argparse.ArgumentParser()
args = parse_args(ap)
    
def save_model_locally():
    print("The model is saved locally... Please copy the below address and paste in browser to download compiled model")
    print("====================================================")
    print("MODEL DOWNLOAD LINK: http://127.0.0.1:8060/download")
    print("====================================================")
    app.run(port=8060)


@app.route('/download')
def downloadFile():
    path = args["output_file"]
    return send_file(path, as_attachment=True)

if __name__=="__main__":

    if args["platform"] == "alibaba_cloud":
        cloud_agent = AlibabaAgent()
        cloud_agent.download(args["input_bucket"], args["input_file"], "trained_model/fused_model.onnx")
        onnx_to_tvm("trained_model/fused_model.onnx", "compiled_model.tar.gz")
        cloud_agent.upload(args["output_bucket"], "compiled_model.tar.gz", args["output_file"])
        
    elif args["platform"] == "aws":
        cloud_agent = AwsAgent()
        print("Agent_selected")
        cloud_agent.download_from_s3(args["input_bucket"], args["input_file"], "trained_model/fused_model.onnx")
        print("Downloaded model...Starting compilation.....")
        onnx_to_tvm("trained_model/fused_model.onnx", "compiled_model.tar.gz")
        cloud_agent.upload_to_s3(args["output_bucket"], args["output_file"], "compiled_model.tar.gz")
        
    elif args["platform"] == "local":
        onnx_to_tvm(args["input_file"], args["output_file"])
        save_model_locally()
        
    else:
        print("Invalid_option...")

